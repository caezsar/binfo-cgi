#!/bin/bash

./head.cgi

modules() {
cat << EOF

<body onload="tableSearch.init();">
  
<input type="text"  class="search1" size="auto" placeholder="Type a value" id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />

   <table id="results1" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Module</a></th>
		<th><a class="text-info">Size</a></th>
		<th><a class="text-info">Used by</a></th>	
      </tr>
    </thead>

EOF

echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

item=`lsmod |grep -v Module\
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3" "$4" "$5" "$5" "$6" "$7" "$8" "$9" "$10 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "</table>"

cat << EOF
<div id="pageNavPosition1"></div>
    <script type="text/javascript"><!--
        var pager1 = new Pager1('results1', 8); 
        pager1.init(); 
        pager1.showPageNav1('pager1', 'pageNavPosition1'); 
        pager1.showPage1(1);
    //--></script>
EOF

echo "</section>"
}

cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- IP TABLES 
================================================== -->                      	

                  <div class="panel-body">
          
     

                    
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>



                            <!-- SECOND RAW 
================================================== -->  
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Kernel Modules</a>
            `modules`
        </div>
      </div>  
	  

      
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Hardware - Raw output</a>
            <a  class="list-group-item"><pre><font color=#2D3C86 ><strong> `lshw`  </strong></font></pre></a>
        </div>
      </div>  
	  
        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Hardware - Raw output</a>
            <a  class="list-group-item"><pre><font color=#2D3C86 ><strong> `lspci -vvv`  </strong></font></pre></a>
        </div>
      </div> 
EOF
        
./footer.cgi
         	
                      	
