#!/bin/bash


./head.cgi

ip_conf() {

cat << EOF
  <table class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
	  <th><a class="text-info">Nr</a></th>
        <th><a class="text-info">Interface</a></th>
        <th><a class="text-info">IP</a></th>
        <th><a class="text-info">Up</a></th>
        <th><a class="text-info">Down</a></th>
      </tr>
    </thead>
EOF

count=1
for i in `/sbin/ifconfig -a | cut -d" " -f1| tr -d :`; do
number=$((count++))
int_name=`/sbin/ifconfig $i | /usr/bin/awk '{print $1}'| tr -d : | awk 'NR==1'`
int_ip=`/sbin/ifconfig $i | grep inet | /usr/bin/awk '{print $2}' | /usr/bin/cut -d: -f2`
down=`/sbin/ifconfig $i| grep "RX packets" | head -1 | /usr/bin/awk '{print $6, $7}'| cut -d"(" -f2 | cut -d")" -f1 2>/dev/null`
up=`/sbin/ifconfig $i| grep "TX packets" | head -1 | /usr/bin/awk '{print $6, $7}'| cut -d"(" -f2 | cut -d")" -f1 2>/dev/null`

cat << EOF

    <tbody>
      <tr bgcolor="#ffffff">
	      <td><h6><font color=#2D3C86 ><strong> $number</font><h6></td>
        <td><h6><font color=#2D3C86 ><strong> $int_name</font><h6></td>
        <td><h6><font color=#801395 ><strong>$int_ip</font><h6></td>
        <td><h6><font color=#801C30 ><strong>$up</font><h6></td>
        <td><h6><font color=#117121 ><strong>$down</font><h6></td>       
      </tr>
   
    </tbody>

EOF
done
echo "</table>"
echo "</section>"
}

arp_table() {
cat << EOF

   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Address</a></th>
		<th><a class="text-info">HWtype</a></th>
		<th><a class="text-info">MAC</a></th>		
		<th><a class="text-info">Flags</a></th>
		<th><a class="text-info">Iface</a></th>
      </tr>
    </thead>
EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`arp -n| grep -v Address \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $5 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "</table></section>"
}




routing() {
cat << EOF

   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Destination</a></th>
		<th><a class="text-info">Gateway</a></th>
		<th><a class="text-info">Genmask</a></th>
		<th><a class="text-info">Flags</a></th>		
		<th><a class="text-info">MSS</a></th>
		<th><a class="text-info">Window</a></th>
		<th><a class="text-info">Interface</a></th>
      </tr>
    </thead>
EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`netstat -r | grep -v Kernel| grep -v Destination \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $5 "<td bgcolor=white><font color=blue><h6><strong>" $6 "<td bgcolor=white><font color=blue><h6><strong>" $8 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "</table>"

cat << EOF
<div id="pageNavPosition"></div>
    <script type="text/javascript"><!--
       var pager = new Pager('results', 8); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF
echo "</section>"
}


tcp_servers_numerical() {
cat << EOF

   <table id="results2" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Protocol</a></th>
		<th><a class="text-info">Recv-Q</a></th>
		<th><a class="text-info">Send-Q</a></th>
		<th><a class="text-info">Local Socket</a></th>		
		<th><a class="text-info">Remote Socket</a></th>

                <th><a class="text-info">Pid/Prog</a></th>

      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`./nets.cgi | grep LISTEN |grep -ve Local -ve only \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $5 "<td bgcolor=white><font color=blue><h6><strong>" $7 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "</table>"

cat << EOF
<div id="pageNavPosition2"></div>
    <script type="text/javascript"><!--
        var pager2 = new Pager2('results2', 8); 
        pager2.init(); 
        pager2.showPageNav2('pager2', 'pageNavPosition2'); 
        pager2.showPage2(1);
    //--></script>
EOF
echo "</section>"
}

udp_servers_numerical() {
cat << EOF

   <table id="results2" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Protocol</a></th>
		<th><a class="text-info">Recv-Q</a></th>
		<th><a class="text-info">Send-Q</a></th>
		<th><a class="text-info">Local Socket</a></th>		
		<th><a class="text-info">Remote Socket</a></th>
 <th><a class="text-info">Pid/Prog</a></th>

      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`./nets.cgi | grep -ve LISTEN -ve Local -ve only \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $5 "<td bgcolor=white><font color=blue><h6><strong>" $6 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "</table>"
echo "</section>"
}

net_conn() {
cat << EOF

<body onload="tableSearch.init();">
  
<input type="text"  class="search1" size="auto" placeholder="Type a value" id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />

   <table id="results1" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Nr</a></th>
		<th><a class="text-info">Protocol</a></th>
		<th><a class="text-info">Local Socket</a></th>	
		<th><a class="text-info">Remote Socket</a></th>		
		<th><a class="text-info">State</a></th>	
		<th><a class="text-info">Timer</a></th>	
      </tr>
    </thead>

EOF

echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

item=`netstat -tuno  | grep -v "servers" | grep -v "Address" | sort -nr | uniq -c \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $5 "<td bgcolor=white><font color=blue><h6><strong>" $6 "<td bgcolor=white><font color=blue><h6><strong>" $7 "<td bgcolor=white><font color=blue><h6><strong>" $8 $9 $10 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "</table>"

cat << EOF
<div id="pageNavPosition1"></div>
    <script type="text/javascript"><!--
        var pager1 = new Pager1('results1', 8); 
        pager1.init(); 
        pager1.showPageNav1('pager1', 'pageNavPosition1'); 
        pager1.showPage1(1);
    //--></script>
EOF

echo "</section>"
}



cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	                      
                            <!-- IP TABLES 
================================================== -->                      	

                  <div class="panel-body">
EOF
 
cat << EOF
                    
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>
                            <!-- SECOND RAW 
================================================== -->  

        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Interface Table</a>
             `ip_conf` 
        </div>
      </div>

<!-- Comment tcp servers

        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Servers Name</a>
             tcp_servers
        </div>
      </div>
--> 

         <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">TCP Listening Servers </a>
             `tcp_servers_numerical` 
        </div>
      </div>
         <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">UDP Listening Servers </a>
             `udp_servers_numerical` 
        </div>
      </div>
	  
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Connections - Table output</a>
            `net_conn`
        </div>
      </div>  

        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Kernel Route Table</a>
             `routing`
        </div>
      </div>

        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Arp Table</a>
             `arp_table`
        </div>
      </div>	  
	  
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active" href="./traffic.cgi">Traffic Info - Raw output</a>
            <a  class="list-group-item"><pre><strong><font color=#2D3C86> `vnstat` </font></strong></pre></a>
        </div>
      </div>

    
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Network Interfaces - Raw output</a>
            <a  class="list-group-item"><pre><strong><font color=#2D3C86> `/sbin/ifconfig` </font></strong></pre></a>
        </div>
      </div>

       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Servers - Raw output</a>
            <a  class="list-group-item"><pre><strong><font color=#2D3C86> `./nets.cgi` </font></strong></pre></a>
		
        </div>
		
		
	    <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Kernel Routing Table - Raw output</a>
            <a  class="list-group-item"><pre><strong><font color=#2D3C86> `route -n` </font></strong></pre></a>
        </div>	
		
		
      </div>	  
EOF
        
./footer.cgi
