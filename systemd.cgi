#!/bin/bash

distribution=`lsb_release -c| awk '{print $2}'`
./head.cgi


systemd_services() {
cat << EOF


   <table id="results1" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Unit</a></th>
		<th><a class="text-info">State</a></th>		
		<th><a class="text-info">Description</a></th>	
		</tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`systemctl list-units -t service --no-legend| sed 's/running/<font\/color=green>running/g'| sed 's/exited/<font\/color=red>exited/g'\
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $5" "$6" "$7" "$8" "$9" "$10"</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition1"></div>
    <script type="text/javascript"><!--
        var pager1 = new Pager1('results1', 8); 
        pager1.init(); 
        pager1.showPageNav1('pager1', 'pageNavPosition1'); 
        pager1.showPage1(1);
    //--></script>
EOF

echo "</section>"
}



systemd_units() {
cat << EOF
<body onload="tableSearch.init();">
  
<input type="text"  class="search1" size="auto" placeholder="Type value"  id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />
   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Unit</a></th>
		<th><a class="text-info">State</a></th>		
      </tr>
    </thead>

EOF
echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

item=`systemctl list-unit-files -t service --no-legend | sed 's/enabled/<font\/color=green>enabled/g'| sed 's/disabled/<font\/color=red>disabled/g'| sed 's/static/<font\/color=orange>static/g' | grep -v "unit"\
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition"></div>
    <script type="text/javascript"><!--
        var pager = new Pager('results', 8); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF

echo "</section>"
}


cat << EOF


                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                                                      
</div><!--/col-->
 </div><!--/row-->

                            <!-- SECOND RAW 
================================================== -->   

  <div class="row">



	  
	   <div class="panel-body">
          <div class="list-group">
		
            <a  class="list-group-item active">Total Services: `systemctl list-unit-files -t service --no-legend| wc -l` - Table output</a>
           `systemd_services`
        </div>
      </div> 


	   <div class="panel-body">
          <div class="list-group">
		
            <a  class="list-group-item active">Table output</a>
           `systemd_units`
        </div>
      </div> 

EOF
        
./footer.cgi
