#!/bin/bash

date=`date '+%b'`
APACHE_ACCESS_LOG_FILE="/var/log/apache2/access.log"
path="/var/www/html/binfo-cgi/visitors/"

case $date in

Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)

cd $path


if [ -f $date/visitors.js ]; then

day=`date -d "1 day ago" '+%d/%b/%Y'`
date1=`date -d "1 day ago" '+%Y-%m-%d'`
ytraf=`cat $APACHE_ACCESS_LOG_FILE | grep $day| grep -v Googlebot | grep -v bingbot |awk '{print $1}' | sort -nr | uniq -c | wc -l`

cd $date
cat visitors.js | sed "s/],$/{ X: \"$date1\", Y: $ytraf },\n&/" visitors.js > visitors1.js
cp visitors1.js visitors.js


else
mkdir $date
cp visitors.html $date/index.html
cp visitors.js $date/visitors.js

day=`date -d "1 day ago" '+%d/%b/%Y'`
date1=`date -d "1 day ago" '+%Y-%m-%d'`
ytraf=`cat $APACHE_ACCESS_LOG_FILE | grep $day| grep -v Googlebot | grep -v bingbot |awk '{print $1}' | sort -nr | uniq -c | wc -l`

cd $date
cat visitors.js | sed "s/],$/{ X: \"$date1\", Y: $ytraf },\n&/" visitors.js > visitors1.js
cp visitors1.js visitors.js

fi
;;

*)
exit 0
;;
esac

