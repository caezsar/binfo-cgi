#!/bin/bash


./head.cgi

### START TABLE FUNCTION ###
packages() {
cat << EOF

<body onload="tableSearch.init();">
  
<input type="text"  class="search1" size="auto" placeholder="Enter package name" id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />
 
   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Program</a></th>
		<th><a class="text-info">Version</a></th>
		<th><a class="text-info">Arch</a></th>	
		<th><a class="text-info">Description</a></th>		
      </tr>
    </thead>

EOF
##Start table with data##
echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

##Get rest of table from awk script###

item=`dpkg -l | grep -v  Name | grep -v Err? | grep -v Status | grep -v Desired | grep -v +++- \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $5" "$6" "$7" "$8" "$9" "$10" "$11" "$12" "$13"</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

##Finish table###
echo "  </table>"

##Put pagination##
cat << EOF
<div id="pageNavPosition"></div>
    <script type="text/javascript"><!--
        var pager = new Pager('results', 20); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF

echo "</section>"
}
### END TABLES FUNCTION ###


cat << EOF


                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- IP TABLES 
================================================== -->                      	

                  <div class="panel-body">
 
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

    
        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Total Installed Packages: `dpkg -l | wc -l` - Table output</a>
           `packages`
        </div>
      </div> 
 
EOF
        
./footer.cgi
