#!/bin/bash

distribution=`lsb_release -c| awk '{print $2}'`
./head.cgi

one=`/bin/cat /proc/loadavg | /usr/bin/awk '{print $1}'`
rez_one=`echo $one*100|bc`

_5=`/bin/cat /proc/loadavg | /usr/bin/awk '{print $2}'`
rez_5=`echo $_5*100|bc`

_15=`/bin/cat /proc/loadavg | /usr/bin/awk '{print $3}'`
rez_15=`echo $_15*100|bc`

runlev=`/sbin/runlevel | awk '{print $2}'`
boot_services=`ls /etc/rc"$runlev".d/ | grep -v README | cut -b 4-30| grep -v [0-9]|wc -l`



cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- CPU USAGE
================================================== -->                      	
                      	
                
              <div class="panel panel-default">
                  <div class="panel-heading"><h4> System Load <a class="text-danger"><h5>  </h5></a></h4></div>
                  <div class="panel-body">
                    
                    <small> <a class="text-success"> `echo $one`% - 1min </a></small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow=" " aria-valuemin="0" aria-valuemax="100" style="width: `echo $rez_one`%">             
                      </div>
                    </div>


                    <small> <a class="text-danger">`echo $_5`% -  5 min </a></small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow=" " aria-valuemin="0" aria-valuemax="100" style="width: `echo $rez_5`%">
                      </div>
                    </div>

            
                    
            <small> <a class="text-info"> `echo $_15`%  - 15 min </a></small>
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow=" " aria-valuemin="0" aria-valuemax="100" style="width: `echo $rez_15`%">     
                      </div>
                    </div>    
                    
</div><!--/panel-body-->
</div><!--/panel-->                                  
</div><!--/col-->
 </div><!--/row-->

                            <!-- SECOND RAW 
================================================== -->  

  <div class="row">

        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">System Load - Raw output</a>
            <pre><strong><font color=#2D3C86> `uptime` </font></strong></pre>
        </div>
      </div>
EOF
      
     case  $distribution  in
                jessie|vivid|xenial|stretch)  

servd_status() {
cat << EOF

<body onload="tableSearch.init();">
  
<input type="text"  class="search1" size="auto" placeholder="Type value"  id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />

   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Service</a></th>
		<th><a class="text-info">Enabled</a></th>
		<th><a class="text-info">Status</a></th>
      </tr>
    </thead>

EOF

echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

item=`./systemds|grep -v grep\
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3" "$4" "$5" "$6" "$7" "$8" "$10" "$11" "$12 " </td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition"></div>   
    <script type="text/javascript"><!--
        var pager = new Pager('results', 10); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF

echo "</section>"
}

				
cat << EOF				

	  
	  
	   <div class="panel-body">
          <div class="list-group">
		
            <a  class="list-group-item active">Total Services: `systemctl list-unit-files -t service --no-legend| wc -l` - Table output</a>
           `servd_status`
        </div>
      </div> 
EOF

					;;
                Ubuntu14.04.2LTS|Ubuntu14.10)


serv_status() {
cat << EOF

<body onload="tableSearch.init();">
  
<input type="text"  class="search1" size="auto" placeholder="Type value"  id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />

   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Service</a></th>
		<th><a class="text-info">Enabled</a></th>
		<th><a class="text-info">Status</a></th>
      </tr>
    </thead>

EOF

echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

item=`./services_status|grep -v file \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3" "$4" "$5" "$6" "$7" "$8" "$10" "$11" "$12 " </td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition"></div>   
    <script type="text/javascript"><!--
        var pager = new Pager('results', 10); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF

echo "</section>"
}
				
cat << EOF
	   <div class="panel-body">
          <div class="list-group">
		
            <a  class="list-group-item active">$boot_services Boot Services Status on Runlevel `/sbin/runlevel` - Table output</a>
           `serv_status`
        </div>
      </div> 
EOF

                    ;;            
      
                *)   
	
	;;			
          esac 	  

cat << EOF	  

	  
        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Installed Services - <strong> init.d </strong> - `ls -l /etc/init.d/ | grep x | wc -l` - Raw output</a>
            <pre><font color=#2D3C86 ><strong> `ls -l /etc/init.d/ | grep x | awk '{print $9}'` </font></strong></pre>
        </div>
      </div> 
	  
EOF
        
./footer.cgi
