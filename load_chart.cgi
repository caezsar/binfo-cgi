#!/bin/bash

month=`date '+%b'`
day=`date '+%d'`

loads() {

cd ./load/$month
current_day=`ls -d * | grep $day`
list_days() {
for i in `ls -d *|grep -v index.cgi`;
do
echo "<a href="./load/$month/$i" style='color:#347B9C'>&nbsp;$i</a>"
done

}
cat << EOF
       <div class="panel-body">
          <div class="list-group">
            <a class="list-group-item active"> $current_day $month - System Load Graphs</a>
<a style='color:#347B9C' href=./lmonth_charts.cgi><strong>|List Months|</strong></a>&nbsp;`list_days`
 <pre><iframe src="./load/$month/$current_day" style="border:none" height="470" width="100%"></iframe></pre>

        </div>
      </div> 
EOF
}


rams() {
cd ./ram/$month
current_day=`ls -d * | grep $day`
list_days() {
for i in `ls -d *|grep -v index.cgi` ;
do
echo "<a href="./ram/$month/$i" style='color:#347B9C'>&nbsp;$i</a>"
done

}
cat << EOF
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active"> $current_day $month - System RAM Graphs</a>
<a style='color:#347B9C' href=./rmonth_charts.cgi><strong>|List Months|</strong></a>&nbsp;`list_days`
 <pre><iframe src="./ram/$month/$current_day" style="border:none" height="470" width="100%"></iframe></pre>

        </div>
      </div> 
EOF
}


cpus() {

cd ./cpu/$month
current_day=`ls -d * | grep $day`
list_days() {
for i in `ls -d *|grep -v index.cgi` ;
do
echo "<a href="./cpu/$month/$i" style='color:#347B9C'>&nbsp;$i</a>"
done

}
cat << EOF
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active"> $current_day $month - System CPU Graphs</a>
<a style='color:#347B9C' href=./cmonth_charts.cgi><strong>|List Months|</strong></a>&nbsp;`list_days`
 <pre><iframe src="./cpu/$month/$current_day" style="border:none" height="470" width="100%"></iframe></pre>

        </div>
      </div> 
EOF
}

./head.cgi

if test -d ./cpu/$month && test -d ./ram/$month && test -d ./load/$month; then

cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
       	
                            <!-- IP TABLES 
================================================== -->                      	

                  <div class="panel-body">
     
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

                            <!-- SECOND RAW 
================================================== -->  
	    <div class="panel-body">
          <div class="list-group">
            <a href=./disks_chart.cgi class="list-group-item active"> $current_day $month - Disk Charts</a>
        </div>
      </div> 
      
       <div class="panel-body">
          <div class="list-group">
            `loads`
        </div>
      </div>  

	  
	       
       <div class="panel-body">
          <div class="list-group">
            `rams`
        </div>
      </div>  
	  
	      <div class="panel-body">
          <div class="list-group">
            `cpus`
        </div>
      </div>   

EOF
else


cat << EOF

                <div class="padding">
                    <div class="full">                   
                        <!-- content -->                      
                      	<div class="row">
          	
                  <div class="panel-body">  
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->
<div>

	    <div class="panel-body">
          <div class="list-group">
            <a href=./disks_chart.cgi class="list-group-item active"> $current_day $month - Disk Charts</a>
        </div>
      </div> 

       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active"> $current_day $month - System Charts</a>
<a style='color:#347B9C'><strong>No graphs generated so far! Plase update your crontab file with the following code:</strong></a>
 <p><pre>*/30 * * * * /var/www/html/binfo-cgi/run-charts > /dev/null 2>&1 </pre></p>

        </div>
      </div> 


EOF
fi

./footer.cgi