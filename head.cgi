#!/bin/bash
echo "Content-type: text/html"
echo ""

runlev=`/sbin/runlevel | awk '{print $2}'`

APACHE_ACCESS_LOG_FILE=`cat ./apache_log.txt`
uniq=`cat $APACHE_ACCESS_LOG_FILE | /usr/bin/awk '{print $1}'| sort | uniq -c |wc -l`

cat << EOF
<!DOCTYPE html>
<html lang="en">

                              <!-- HEAD
================================================== -->   

	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>System Information for `hostname -f` </title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/icons.css" />
		
		<script type="text/javascript" src="js/paging.js"></script>
		<script type="text/javascript" src="js/paging1.js"></script>	
		<script type="text/javascript" src="js/paging2.js"></script>
		<script type="text/javascript" src="js/search.js"></script>	
		<script type="text/javascript" src="js/search1.js"></script>	

		
<link rel="stylesheet" href="windowfiles/dhtmlwindow.css" type="text/css" />
<script type="text/javascript" src="windowfiles/dhtmlwindow.js"></script>

	</head>

                              <!-- BODY
================================================== -->   

	<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <!-- sidebar -->
            <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar">

              	<ul class="nav">
<li><a href="#" data-toggle="offcanvas" class="visible-xs text-center"><i class="metro"><span class="icon-arrow-right"></span></i></a></li>
            	</ul>
              
                                <!-- RIGHT VERTICAL MENU
===================================================================================================================== -->       
               
                <ul class="nav hidden-xs" id="lg-menu">

				<li><a class="metro" href="./charts_disk.cgi"><span class="icon-chart-alt"></span> Disk Chart </a></li>	
				<li><a class="metro" href="./interfaces.cgi"><span class="icon-chart-alt"></span> Net Chart </a></li>							
				<li><a class="metro" href="./ram.cgi"><span class="icon-power"></span> RAM Usage </a></li>				
				<li><a class="metro" href="./load.cgi"><span class="icon-loading"></span> System Load </a></li>	
				<li><a class="metro" href="./cpu.cgi"><span class="icon-busy"></span> CPU Usage </a></li>
				<li><a class="metro" href="./disk.cgi"><span class="icon-laptop"></span> Disk Usage </a></li>			   
				<li><a class="metro" href="./diskstat.cgi"><span class="icon-bars"></span> Disk Stats </a></li>	
				<li><a class="metro" href="./net.cgi"><span class="icon-broadcast"></span> Network </a></li>	
				<li><a class="metro" href="./process.cgi"><span class="icon-spin"></span> Processes </a></li>	
				<li><a class="metro" href="./users.cgi"><span class="icon-user"></span> Users </a></li>	

                </ul>



                <ul class="list-unstyled hidden-xs" id="sidebar-footer">
                    <li>
		       <h6> Runlevel: `/sbin/runlevel;echo \|; systemctl get-default` </h6>
               <h6> Uptime: `uptime | cut -d"p" -f2|cut -d"," -f1`</h6> 
			   <h6> `who -b` </h6>
 <h6> Boot Services: `ls /etc/rc$runlev.d/ | grep S | wc -l` / `ls /etc/rc$runlev.d/ | grep -v README | wc -l`  </h6>
					</li>
                </ul>   

              <ul class="nav visible-xs" id="xs-menu">
				<li><a class="metro" href="./ram.cgi"><span class="icon-power"></span></a></li>				
				<li><a class="metro" href="./load.cgi"><span class="icon-loading"></span></a></li>	
				<li><a class="metro" href="./cpu.cgi"><span class="icon-busy"></span></a></li>
				<li><a class="metro" href="./disk.cgi"><span class="icon-laptop"></span></a></li>			   
				<li><a class="metro" href="./diskstat.cgi"><span class="icon-bars"></span></a></li>	
				<li><a class="metro" href="./net.cgi"><span class="icon-broadcast"></span></a></li>	
				<li><a class="metro" href="./process.cgi"><span class="icon-spin"></span></a></li>	
				<li><a class="metro" href="./users.cgi"><span class="icon-user"></span></a></li>	
                <li><a class="metro" href="./a-logs.cgi"><span class="icon-wordpress"></span></a></li>	
				<li><a class="metro" href="./ "><span class="icon-compass"></span></a></li>	
				<li><a class="metro" href="./charts_disk.cgi"><span class="icon-calculate"></span></a></li>
				<li><a class="metro" href="./packages.cgi"><span class="icon-box"></span></a></li>
				<li><a class="metro" href="./auth_failed.cgi"><span class="icon-locked"></span></a></li>
				
                </ul>
				
            </div>


                                     <!--    UP NAVIGATION BAR BLUE
========================================================================================================================== -->      


            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
              	<div class="navbar navbar-blue navbar-static-top">  
                    <div class="navbar-header">
					
                      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
          				<span class="icon-bar"></span>
          				<span class="icon-bar"></span>
                      </button>
                 <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle</span>
<a target=_blank href="http://`echo $SERVER_ADDR`"><span class="badge"> `echo $SERVER_ADDR` </span></a>
<a><span class="badge"> $uniq </span></a>
                      </button>
                      
                 	
                  	</div>
                  	<nav class="collapse navbar-collapse" role="navigation">
                   
                    <ul class="nav navbar-nav">
					
 
		     <li>
                        <a href="./index.cgi" class="metro" ><span class="badge"><span  class="icon-tux">  `cat /etc/issue.net` </span></span></a>
                      </li>
		     <li>
                        <a href="./charts_disk.cgi"><span class="badge"> `uname -sr` </span></a>
                      </li>
                      <li>
                        <a target=_blank href="http://`hostname -f`"> @ `hostname -f`</a>
                      </li>
		     <li>
<a><span class="badge"> $uniq </span></a>
             </li>					  
				       <li>
                        <a target=_blank href="http://`echo $SERVER_ADDR`"><span class="badge"> `echo $SERVER_ADDR` </span></a>
                      </li>	  

              <li class="dropdown">
                        <a href="#" class="metro" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-plus-2"></span> Menu </a>
                        <ul class="dropdown-menu">
				<li><a class="metro" href="./charts_disk.cgi"><span class="icon-calculate"></span> Disk Charts </a></li>
				<li><a class="metro" href="./ "><span class="icon-compass"></span> System Charts </a></li>	
				<li><a class="metro" href="./load_chart.cgi "><span class="icon-compass"></span> Load Charts </a></li>
				<li><a class="metro" href="./visitors.cgi "><span class="icon-bars"></span> Visitors Charts </a></li>				
				<li><a class="metro" href="./ram.cgi"><span class="icon-power"></span> RAM Usage </a></li>				
				<li><a class="metro" href="./load.cgi"><span class="icon-loading"></span> System Load </a></li>	
				<li><a class="metro" href="./cpu.cgi"><span class="icon-busy"></span> CPU Usage </a></li>
				<li><a class="metro" href="./disk.cgi"><span class="icon-laptop"></span> Disk Usage </a></li>			   
				<li><a class="metro" href="./diskstat.cgi"><span class="icon-bars"></span> Disk Stats </a></li>	
				<li><a class="metro" href="./net.cgi"><span class="icon-broadcast"></span> Network </a></li>	
				<li><a class="metro" href="./process.cgi"><span class="icon-spin"></span> Processes </a></li>	
				<li><a class="metro" href="./users.cgi"><span class="icon-user"></span> Users </a></li>
				<li><a class="metro" href="./a-logs.cgi"><span class="icon-wordpress"></span> Apache </a></li>
				<li><a class="metro" href="./packages.cgi"><span class="icon-box"></span> Packages </a></li>
				<li><a class="metro" href="./auth_failed.cgi"><span class="icon-locked"></span> SSH Failed </a></li>
				<li><a class="metro" href="./hardware.cgi"><span class="icon-locked"></span> Hardware </a></li>
				<li><a class="metro" href="./logs.cgi"><span class="icon-help"></span> dmesg </a></li>
                        </ul>
                      </li>             

                     <li>
                        <a target=_blank href="http://`echo $REMOTE_ADDR`"># `echo $REMOTE_ADDR` </a>
                      </li> 

                    </ul>

                              <!-- NAV BAR BLUE RIGHT ITEMS
================================================== -->   

                    <ul class="nav navbar-nav navbar-right">
				
                      <li class="dropdown">
					 
                        <a href="#" class="metro" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-arrow-down"></span></a>
                        <ul class="dropdown-menu">
						 
                <li><a class="metro" href="./env.cgi"><span class="icon-support"></span> Environment </a></li>
				    <li><a class="metro" href="./sys_logs.cgi"><span class="icon-support"></span> Syslogs </a></li>
    <li><a class="metro" href="./livelog.cgi"><span class="icon-support"></span> Livelog </a></li>
	    <li><a class="metro" href="./systemd.cgi"><span class="icon-support"></span> Systemd </a></li>
                        </ul>
                      </li>
					  
					   <li class="dropdown">
                        <a target="_blank" href="https://github.com/caezsar/binfo-cgi" class="metro"><span class="icon-github"></span></a>
                      </li>
					  
                    </ul>
                  	</nav>
                </div>

                <!-- /top nav -->
      
 
EOF
