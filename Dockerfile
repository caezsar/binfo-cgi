FROM atlassian/default-image:2
MAINTAINER caezsar@yahoo.com 

RUN apt-get update
#RUN apt-get install -y apt-utils
RUN apt-get install -y apache2 net-tools sysstat vnstat wget bc lynx
RUN echo 'ServerName localhost' >> /etc/apache2/apache2.conf
WORKDIR /var/www/html/
COPY . ./ 
RUN chown -R root:www-data . && chmod -R 755 .
#COPY script.sh /root/
#RUN chmod 755 /root/script.sh
#RUN ls -l /root/
RUN ./script.sh
RUN a2enmod cgi && a2dissite 000-default && a2ensite cgi 

RUN service apache2 restart && curl http://localhost/cpu.cgi | grep 'CPU COMMAND' -A 10 || (echo 'pipeline failed' && exit 1)
#CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]
#CMD ["echo","mage created"] 
#RUN curl http://localhost/
