#!/bin/bash


int="`/sbin/ifconfig|grep -i 'eth[0-9]\|en[p\|o\|s]\|bridge\|v' | cut -d" " -f1| head -1| tr -d :`"

charts() {

## LOAD ##
one=`/bin/cat /proc/loadavg | /usr/bin/awk '{print $1}'`
rez_one=`echo $one*100|bc`
_5=`/bin/cat /proc/loadavg | /usr/bin/awk '{print $2}'`
rez_5=`echo $_5*100|bc`
_15=`/bin/cat /proc/loadavg | /usr/bin/awk '{print $3}'`
rez_15=`echo $_15*100|bc`

## RAM
total=`free -m | awk 'NR==2' | awk '{ printf $2}'`
used=`free -m | awk 'NR==2' | awk '{ printf $3}'`
used_percent=`echo "$used*100/$total" | bc`

free=`free -m | awk 'NR==2' | awk '{ printf $4}'`
free_percent=`echo "$free*100/$total" | bc`

cached=`free -m | awk 'NR==2' | awk '{ printf $7}'`
cached_percent=`free -m | awk '/Mem/ { printf("%3.1f%%", $7/$2*100) }'`

buff=`free -m | awk 'NR==2' | awk '{ printf $6}'`
buff_percent=`free -m | awk '/Mem/ { printf("%3.1f%%", $6/$2*100) }'`

shared=`free -m | awk 'NR==2' | awk '{ printf $5}'`
shared_percent=`free -m | awk '/Mem/ { printf("%3.1f%%", $5/$2*100) }'`


## CPU ##

idle=`iostat | awk 'NR==4' | awk '{ printf $6}'`
rez_idle=`echo $idle*100/1|bc`
user=`iostat | awk 'NR==4' | awk '{ printf $1}'`
rez_user=`echo $user*100/1|bc`
sys=`iostat | awk 'NR==4' | awk '{ printf $3}'`
rez_sys=`echo $sys*100/1|bc`
iowait=`iostat | awk 'NR==4' | awk '{ printf $4}'`
rez_iowait=`echo $iowait*100/1|bc`
nice=`iostat | awk 'NR==4' | awk '{ printf $2}'`
rez_nice=`echo $nice*100/1|bc`

## NET

val_tx=`/sbin/ifconfig $int | grep "TX packets" | cut -d: -f3 | awk '{print $5}'`
rez_tx=`echo $val_tx/1000000|bc`
txh=`/sbin/ifconfig $int |grep "TX packets" | awk '{print $5, $6, $7}'`


val_rx=`/sbin/ifconfig $int | grep "RX packets" | cut -d: -f2 | awk '{print $5}'`
rez_rx=`echo "$val_rx/1000000"|bc`
rxh=`/sbin/ifconfig $int | grep "RX packets" | awk '{print $5, $6, $7}'`

val_total=`echo "$val_rx+$val_tx"|bc`
rez_totaln=`echo "$val_total/1000000"|bc`
totaln=`echo "$rez_totaln/1000000"|bc`


cat << EOF


<script type="text/javascript" src="canvas/canvasjs.js"></script>

   
   <script type="text/javascript">
  window.onload = function () {
 

	
 
var chart = new CanvasJS.Chart("chartContainer6",
    {
        animationEnabled: true,
        title: {
            text: "HDD Chart",
        },
        data: [
        {
            type: "pie",
            showInLegend: true,
            dataPoints: [

EOF
		
item=`df -m | grep -v Filesystem|grep "^/"\
| awk ' {}{line[j++] = "{ y: "$2", legendText: \" "$6" Size: "$2" MB \", indexLabel: \" Used: "$5 " " $6 " - " $1 " \" }, "}END{ for(i=0;i<j;i++) print line[i]; }'`

echo "$item"

cat << EOF
			]
        },
        ]
    });
chart.render();
	

	

 var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "System Load",   
      },
      axisY: {
        title: "Total"
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme1",
      data: [
 
      {       
        type: "column", 
        showInLegend: false,
        legendMarkerColor: "grey",
        legendText: " System Load ",
        dataPoints: [     
        {y: 100, label: "Loadav 1.00"},		
        {y: `echo $rez_one`, label: "1 Minute: `echo $one`% "},
        {y: `echo $rez_5`,  label: "5 Minutes: `echo $_5`% " },
        {y: `echo $rez_15`,  label: "15 Minutes `echo $_15`% "},       
        ]
      },  
      ]
    });
 
    chart.render();
	


	
    var chart = new CanvasJS.Chart("chartContainer1",
    {
      title:{
        text: "RAM Usage",   
      },
      axisY: {
        title: "Total "
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme1",
      data: [
 
      {       
        type: "column", 
        showInLegend: false,
        legendMarkerColor: "grey",
        legendText: "RAM Usage ",
        dataPoints: [     
        {y: `echo $total`, label: "Total: `echo $total` Mb "},
        {y: `echo $free`,  label: "Free `echo $free` Mb - `echo $free_percent`% "},     		
        {y: `echo $used`,  label: "Used: `echo $used` Mb - `echo $used_percent`% " },
        {y: `echo $cached`,  label: "Avail: `echo $cached` Mb - `echo $cached_percent` " },
        {y: `echo $buff`,  label: "Buffered: `echo $buff` Mb - `echo $buff_percent` " },	
        {y: `echo $shared`,  label: "Shared: `echo $shared` Mb - `echo $shared_percent` " },			
        ]
      },  
      ]
    });
 
    chart.render();	
	



    var chart = new CanvasJS.Chart("chartContainer2",
    {
      title:{
        text: "CPU Load",   
      },
      axisY: {
        title: "Total "
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme2",
      data: [
 
      {       
        type: "column", 
        showInLegend: false,
        legendMarkerColor: "grey",
        legendText: "CPU Load ",
        dataPoints: [     
        {y: `echo 10000`, label: "Total: 100% "},
        {y: `echo $rez_idle`,  label: "Idle `echo $idle`% "},     		
        {y: `echo $rez_user`,  label: "User: `echo $user`% " },
        {y: `echo $rez_sys`,  label: "System: `echo $sys`% " },
        {y: `echo $rez_iowait`,  label: "Iowait: `echo $iowait`% " },	
        {y: `echo $rez_nice`,  label: "Nice: `echo $nice`% " },			
        ]
      },  
      ]
    });
 
    chart.render();	
	
	

    var chart = new CanvasJS.Chart("chartContainer3",
    {
      title:{
        text: "`echo $int` Traffic",   
      },
      axisY: {
        title: "Total "
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme3",
      data: [
 
      {       
        type: "column", 
        showInLegend: false,
        legendMarkerColor: "grey",
        legendText: "`echo $int` Traffic",
        dataPoints: [     
        {y: `echo $rez_totaln`, label: "Total: `echo $rez_totaln` MB "},
        {y: `echo $rez_tx`,  label: "Transmitted: `echo $txh` "},     		
        {y: `echo $rez_rx`,  label: "Received: `echo $rxh` " },		
        ]
      },  
      ]
    });
 
    chart.render();		
	
	
	
var chart = new CanvasJS.Chart("chartContainer4",
    {
        animationEnabled: true,
        title: {
            text: "RAM Chart",
        },
        data: [
        {
            type: "pie",
            showInLegend: true,
            dataPoints: [
                { y: `echo $total`, legendText: "Total: `echo $total` Mb " , indexLabel: "`echo $total` Mb"},
                { y: `echo $free`, legendText: "Free `echo $free` Mb - `echo $free_percent`%", indexLabel: "`echo $free` Mb" },
                { y: `echo $used`, legendText: "Used `echo $used` Mb - `echo $used_percent`%", indexLabel: "`echo $used` Mb" },				
                { y: `echo $cached`, legendText: "Avail `echo $cached` Mb - `echo $cached_percent`", indexLabel: "`echo $cached` Mb" },
                { y: `echo $buff`, legendText: "Buffered `echo $buff` Mb - `echo $buff_percent`", indexLabel: "`echo $buff` Mb" },
                { y: `echo $shared`, legendText: "Shared `echo $shared` Mb - `echo $shared_percent`", indexLabel: "`echo $shared` Mb" },
            ]
        },
        ]
    });
chart.render();


var chart = new CanvasJS.Chart("chartContainer5",
    {
        animationEnabled: true,
        title: {
            text: "Load Chart",
        },
        data: [
        {
            type: "pie",
            showInLegend: true,
            dataPoints: [
                { y: 100, legendText: " Total Loadav 1.00", indexLabel: "Loadav 1.00" },			
                { y: `echo $rez_one`, legendText: "1 Minute: `echo $one` " , indexLabel: "1 Minute: `echo $one` "},
                { y: `echo $rez_5`, legendText: "5 Minutes `echo $_5`" , indexLabel: "5 Minutes: `echo $_5`" },
                { y: `echo $rez_15`, legendText: "15 Minutes `echo $_15`" , indexLabel: "15 Minutes: `echo $_15`" },				
            ]
        },
        ]
    });
chart.render();

	

  }
  </script>

 
EOF

}



## Run charts function
charts


cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- MEMORY RAM 
================================================== -->                      	
      
                  <div class="panel-body">

</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

                           <!-- SECOND RAW 
================================================== -->  
        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">RAM Statistics</a>
		 <div id="chartContainer4" style="height: 400px; width: 100%;"> </div>
        </div>
      </div>
	  
        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">RAM Statistics</a>
		 <div id="chartContainer1" style="height: 300px; width: 100%;"> </div>
        </div>
      </div>

        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">System Load</a>
		 <div id="chartContainer5" style="height: 350px; width: 100%;"> </div>
        </div>
      </div>


        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">System Load Statistics</a>        
		 <div id="chartContainer" style="height: 300px; width: 100%;"> </div>
        </div>
      </div>


        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">CPU Usage Statistics</a>        
		 <div id="chartContainer2" style="height: 300px; width: 100%;"> </div>
        </div>
      </div>	

	         <div class="panel-body">
          <div class="list-group">
            <a  href="./interfaces.cgi" class="list-group-item active">`echo $int` Statistics</a>        
		 <div id="chartContainer3" style="height: 300px; width: 100%;"> </div>
        </div>
      </div> 
	  

       <div class="panel-body">
          <div class="list-group">
            <a href="./charts_disk.cgi" class="list-group-item active"><strong>Disk Statistics</strong></a>
		 <div id="chartContainer6" style="height: 400px; width: 100%;"> </div>
        </div>
      </div>
	  
EOF
