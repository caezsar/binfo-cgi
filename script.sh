cat << EOF > /etc/apache2/sites-available/cgi.conf

<VirtualHost *:80>
        ServerAdmin webmaster@localhost     
        DocumentRoot /var/www/html
        <Directory />
                Options FollowSymLinks
                AllowOverride All
                Require all granted
        </Directory>
        <Directory /var/www/html>
                Options Indexes FollowSymLinks MultiViews ExecCGI
                AddHandler cgi-script .cgi .pl
                AllowOverride All
                Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/cgi-error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/cgi-access.log combined
</VirtualHost>
EOF

