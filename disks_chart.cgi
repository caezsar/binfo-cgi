#!/bin/bash

month=`date '+%b'`
day=`date '+%d'`



disks() {


cd ./disk/$month/$day/
for i in `ls *.html`; do
cat << EOF
       <div class="panel-body">
          <div class="list-group">
            <a href=./disk/$month/$day/$i class="list-group-item active"> $day $month - Disk Graphs: <strong>$i</strong> </a>
<a style='color:#347B9C' href=./dmonth_charts.cgi><strong>|List Months|</strong></a>&nbsp;
 <pre><iframe src="./disk/$month/$day/$i" style="border:none" height="470" width="100%"></iframe></pre>

        </div>
      </div> 
EOF

done
}


./head.cgi

if test -d ./disk/$month; then

cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full ">
                      
                        <!-- content -->                      
                      	<div class="row">
       	
                            <!-- IP TABLES 
================================================== -->                      	

                  <div class="panel-body">
     
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

                            <!-- SECOND RAW 
================================================== -->  
	    <div class="panel-body">
          <div class="list-group">
            <a href=./load_chart.cgi class="list-group-item active"> $current_day $month - Load Charts</a>
        </div>
      </div> 
	  
	  	      <div class="panel-body">
          <div class="list-group">
            `disks`
        </div>
      </div> 
EOF

else


cat << EOF

                <div class="padding">
                    <div class="full">                   
                        <!-- content -->                      
                      	<div class="row">
          	
                  <div class="panel-body">  
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->
<div>

	    <div class="panel-body">
          <div class="list-group">
            <a href=./load_chart.cgi class="list-group-item active"> $current_day $month - Load Charts</a>
        </div>
      </div> 
	  
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active"> $current_day $month - Disk Graphs</a>
<a style='color:#347B9C'><strong>No graphs generated so far! Plase update your crontab file with the following code:</strong></a>
 <p><pre>*/30 * * * * /var/www/html/binfo-cgi/run-charts > /dev/null 2>&1 </pre></p>

        </div>
      </div> 

EOF
fi

./footer.cgi