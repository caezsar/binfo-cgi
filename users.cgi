#!/bin/bash


./head.cgi

users_online() {
cat << EOF

   <table class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">User</a></th>
		<th><a class="text-info">TTY</a></th>
		<th><a class="text-info">Date</a></th>
		<th><a class="text-info">Hour</a></th>		
		<th><a class="text-info">From</a></th>		
      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`who \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 $4 "<td bgcolor=white><font color=blue><h6><strong>" $5 "<td bgcolor=white><font color=blue><h6><strong>" $6 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"
echo "</section>"
}


#last_logins() {
#result=`last -i`
#        while read -r line; do
#       echo  "<table>"
#        echo "<pre><font color=#2D3C86 ><strong> $line </font></strong>"
#        done <<< "$result"               
#	echo "</pre> </table>"
#}

local_users() {
cat << EOF

   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">User</a></th>
		<th><a class="text-info">UUID</a></th>
		<th><a class="text-info">Home</a></th>
		<th><a class="text-info">Shell</a></th>		
      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`/usr/bin/awk -F: '{ if ($3>=1000) print $1" " $3" " $6" " $7; }' < /etc/passwd | sort -d \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition"></div>   
    <script type="text/javascript"><!--
        var pager = new Pager('results', 8); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF

echo "</section>"
}

system_users() {
cat << EOF

   <table id="results1" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">User</a></th>
		<th><a class="text-info">UUID</a></th>
		<th><a class="text-info">Home</a></th>
		<th><a class="text-info">Shell</a></th>		
      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`/usr/bin/awk -F: '{ if ($3<=499) print $1" " $3" " $6" " $7; }' < /etc/passwd | sort -d \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition1"></div>
    <script type="text/javascript"><!--
        var pager1 = new Pager1('results1', 8); 
        pager1.init(); 
        pager1.showPageNav1('pager1', 'pageNavPosition1'); 
        pager1.showPage1(1);
    //--></script>
EOF

echo "</section>"
}

last_users() {
cat << EOF

   <table id="results2" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">User</a></th>
		<th><a class="text-info">TTY</a></th>
		<th><a class="text-info">From</a></th>
		<th><a class="text-info">Date</a></th>	
		<th><a class="text-info">Login</a></th>	
		<th><a class="text-info">Logout</a></th>
		<th><a class="text-info">Period</a></th>
      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`/usr/bin/last -i | grep -v boot | grep -v wtmp \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 $5 $6 "<td bgcolor=white><font color=blue><h6><strong>" $7 "<td bgcolor=white><font color=blue><h6><strong>" $9 "<td bgcolor=white><font color=blue><h6><strong>" $10 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item


echo "  </table>"

cat << EOF
<div id="pageNavPosition2"></div>
    <script type="text/javascript"><!--
        var pager2 = new Pager2('results2', 8); 
        pager2.init(); 
        pager2.showPageNav2('pager2', 'pageNavPosition2'); 
        pager2.showPage2(1);
    //--></script>
EOF

echo "</section>"
}


cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- IP TABLES 
================================================== -->                      	

                  <div class="panel-body">
 
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

                            <!-- SECOND RAW 
================================================== -->  
    
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Online Users - Table output</a>
            `users_online`
        </div>
      </div>  
    
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Local Users - Table output</a>
            `local_users`
        </div>
      </div> 

       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active"> System Users - Table output</a>
            `system_users`
        </div>
      </div> 
	  
	  
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Last Logins - Raw output</a>
       `last_users`
        </div>
      </div>  


EOF
        
./footer.cgi
