#!/bin/bash

month=`date '+%b'`
day=`date '+%d'`

cd ./disk/$month/$day
list_days() {
for i in `ls -d *.html` ;
do
cat << EOF
            <div align='center'><strong><a href=$i> $i </a></strong></div>
 <pre><iframe src="./$i" style="border:none" height="470" width="100%"></iframe></pre>
<a style='color:#347B9C' href=../><strong>Back</strong></a>

EOF

done
}

cat << EOF

<p>`list_days`</pre>
EOF
