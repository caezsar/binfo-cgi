binfo-cgi
=========

Linux system web info monitor - server monitoring with Apache CGI and Bootstrap 
=======

Binfo-cgi server monitoring - Ubuntu && Debian in raw format.

- Default logs that application reads: `/var/log/apache2/whatever_vhost-access.log`

Install `sysstat, vnstat, wget, bc, net-tools (for debian9, centos7 etc) `tools.

Installation:

- Clone the app into your apache webroot host (Ex: `/var/www/html/`) with using Git cmd line:

`git clone https://github.com/caezsar/binfo-cgi.git`

- Run `cgi-scripts` by adding the following block to your Apache defult or virtual host:

```
<Directory "/var/www/html">
    Options +ExecCGI
    AddHandler cgi-script .cgi .pl
Require all granted
AllowOverride all
</Directory>

```

- Enable cgi scripts with `a2enmod cgi` and restart Apache:

` systemctl restart apache2`


- Visit `http://your_fqdn or server address/binfo-cgi/`

![alt text](https://raw.githubusercontent.com/caezsar/binfo-cgi/master/screen.PNG "screenshot")

![alt text](https://raw.githubusercontent.com/caezsar/binfo-cgi/master/screen1.PNG "screenshot")
![alt text](https://raw.githubusercontent.com/caezsar/binfo-cgi/master/screen2.PNG "screenshot")
![alt text](https://raw.githubusercontent.com/caezsar/binfo-cgi/master/screen3.PNG "screenshot")
![alt text](https://raw.githubusercontent.com/caezsar/binfo-cgi/master/screen4.PNG "screenshot")
