#!/bin/bash


./head.cgi

charts() {

cat << EOF

<script type="text/javascript" src="canvas/canvasjs.js"></script>  
   <script type="text/javascript">
  window.onload = function () {
EOF

for i in `df -m | grep "^/" | awk '{print $6}'`; do

cat << EOF

    var chart = new CanvasJS.Chart("chartContainer_$i",
    {
      title:{
        text: " $i ",   
      },
      axisY: {
        title: "Total "
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme2",
      data: [
 
      {       
        type: "column", 
        showInLegend: false,
        legendMarkerColor: "grey",
        legendText: "$i HDD Usage ",
        dataPoints: [   
EOF
		
size=`df -m  $i | grep -v Filesystem  \
| awk ' {}{line[j++] = "{ y: "$2", label: \" Size: "$2" MB \" }, "}END{ for(i=0;i<j;i++) print line[i]; }'`



used=`df -m $i | grep -v Filesystem \
| awk ' {}{line[j++] = "{ y: "$3", label: \" Used: "$3" MB - "$5" MB \" }, "}END{ for(i=0;i<j;i++) print line[i]; }'`

avail=`df -m $i | grep -v Filesystem \
| awk ' {}{line[j++] = "{ y: "$4", label: \" Free: "$4" MB \" }, "}END{ for(i=0;i<j;i++) print line[i]; }'`



echo $size
echo "$used"
echo "$avail"

cat << EOF
        ]
      },  
      ]
    });
 
    chart.render();	
EOF

done

cat << EOF
  }
  </script>
EOF
## Close the bash function
}


## Run charts function
charts


cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- MEMORY RAM 
================================================== -->                      	
      
                  <div class="panel-body">

</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

                           <!-- SECOND RAW 
================================================== -->  
EOF

for i in `df -m | grep "^/" | awk '{print $6}'`; do

cat << EOF
 
 <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active"><strong> `df -m $i | grep -v Filesystem| awk '{print $1}'` - `df -h $i | grep -v Filesystem| awk '{print $2}'` </strong> </a>
		 <div id="chartContainer_$i" style="height: 300px; width: 100%;"> </div>
        </div>
      </div>
EOF

done

./footer.cgi
