#!/bin/bash

./head.cgi

intf="$(/sbin/ifconfig|grep -i 'eth[0-9]\|en[p\|o\|s]\|bridge\|\tu\|ta\|br\|v' | cut -d" " -f1| tr -d :| sed '/^$/d')"

charts() {


cat << EOF


<script type="text/javascript" src="canvas/canvasjs.js"></script>

   
   <script type="text/javascript">
  window.onload = function () {
 
EOF

for i in $intf; do


val_tx=`/sbin/ifconfig $i | grep "TX packets" | cut -d: -f3 | awk '{print $5}'`
rez_tx=`echo $val_tx/1000000|bc`
txh=`/sbin/ifconfig $i |grep "TX packets" | awk '{print $5, $6, $7}'`


val_rx=`/sbin/ifconfig $i | grep "RX packets" | cut -d: -f2 | awk '{print $5}'`
rez_rx=`echo "$val_rx/1000000"|bc`
rxh=`/sbin/ifconfig $i | grep "RX packets" | awk '{print $5, $6, $7}'`

val_total=`echo "$val_rx+$val_tx"|bc`
rez_totaln=`echo "$val_total/1000000"|bc`
totaln=`echo "$rez_totaln/1000000"|bc`

cat << EOF


    var chart = new CanvasJS.Chart("chartContainer_$i",
    {
      title:{
        text: "`echo $i` Traffic",   
      },
      axisY: {
        title: "Total "
      },
      legend: {
        verticalAlign: "bottom",
        horizontalAlign: "center"
      },
      theme: "theme3",
      data: [
 
      {       
        type: "column", 
        showInLegend: false,
        legendMarkerColor: "grey",
        legendText: "`echo $i` Traffic",
        dataPoints: [     
        {y: `echo $rez_totaln`, label: "Total: `echo $rez_totaln` MB "},
        {y: `echo $rez_tx`,  label: "Transmitted: `echo $txh` "},     		
        {y: `echo $rez_rx`,  label: "Received: `echo $rxh` " },		
        ]
      },  
      ]
    });
 
    chart.render();		
	
EOF
 # end loop
done
	
echo "; } </script>"

# close function
}



## Run charts function
charts


cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- MEMORY RAM 
================================================== -->                      	
      
                  <div class="panel-body">

</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

                           <!-- SECOND RAW 
================================================== -->  
 

EOF

for i in $intf; do   

cat << EOF
 
 <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active"><strong> $i </strong> </a>
		 <div id="chartContainer_$i" style="height: 300px; width: 100%;"> </div>
        </div>
      </div>
EOF

done

./footer.cgi
