function Pager2(tableName2, itemsPerPage2) {
    this.tableName2 = tableName2;
    this.itemsPerPage2 = itemsPerPage2;
    this.currentPage2 = 1;
    this.pages2 = 0;
    this.inited = false;
    
    this.showRecords2 = function(from, to) {        
        var rows = document.getElementById(tableName2).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)  
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
    
    this.showPage2 = function(pageNumber2) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}

        var oldPageAnchor = document.getElementById('pg2'+this.currentPage2);
        oldPageAnchor.className = 'pg-normal2';
        
        this.currentPage2 = pageNumber2;
        var newPageAnchor = document.getElementById('pg2'+this.currentPage2);
        newPageAnchor.className = 'pg-selected2';
        
        var from = (pageNumber2 - 1) * itemsPerPage2 + 1;
        var to = from + itemsPerPage2 - 1;
        this.showRecords2(from, to);
    }   
    
    this.prev = function() {
        if (this.currentPage2 > 1)
            this.showPage2(this.currentPage2 - 1);
    }
    
    this.next = function() {
        if (this.currentPage2 < this.pages2) {
            this.showPage2(this.currentPage2 + 1);
        }
    }                        
    
    this.init = function() {
        var rows = document.getElementById(tableName2).rows;
        var records = (rows.length - 1); 
        this.pages2 = Math.ceil(records / itemsPerPage2);
        this.inited = true;
    }

    this.showPageNav2 = function(pagerName2, positionId) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}
    	var element = document.getElementById(positionId);
    	
    	var pagerHtml2 = '<span onclick="' + pagerName2 + '.prev();" class="pg-normal2"> << </span> ';
        for (var page2 = 1; page2 <= this.pages2; page2++) 
            pagerHtml2 += '<span id="pg2' + page2 + '" class="pg-normal2" onclick="' + pagerName2 + '.showPage2(' + page2 + ');">' + page2 + '</span> ';
        pagerHtml2 += '<span onclick="'+pagerName2+'.next();" class="pg-normal2"> >> </span>';            
        
        element.innerHTML = pagerHtml2;
    }
}

