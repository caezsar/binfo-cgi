
        //define the table search as an object, which can implement both functions and properties
        window.tableSearch1 = {};

        //initialize the search, setup the current object
        tableSearch1.init = function() {
            //define the properties I want on the tableSearch1 object
            this.Rows = document.getElementById('data1').getElementsByTagName('TR');
            this.RowsLength = tableSearch1.Rows.length;
            this.RowsText = [];
            
            //loop through the table and add the data to
            for (var i = 0; i < tableSearch1.RowsLength; i++) {
                this.RowsText[i] = (tableSearch1.Rows[i].innerText) ? tableSearch1.Rows[i].innerText.toUpperCase() : tableSearch1.Rows[i].textContent.toUpperCase();
            }
        }

        //onlys shows the relevant rows as determined by the search string
        tableSearch1.runSearch1 = function() {
            //get the search term
            this.Term = document.getElementById('textBoxSearch1').value.toUpperCase();
            
            //loop through the rows and hide rows that do not match the search query
            for (var i = 0, row; row = this.Rows[i], rowText = this.RowsText[i]; i++) {
                row.style.display = ((rowText.indexOf(this.Term) != -1) || this.Term === '') ? '' : 'none';
            }
        }

        //runs the search
        tableSearch1.search1 = function(e) {
            //checks if the user pressed the enter key, and if they did then run the search
            var keycode;
            if (window.event) { keycode = window.event.keyCode; }
            else if (e) { keycode = e.which; }
            else { return false; }
            if (keycode == 13) {
                tableSearch1.runSearch1();
            }
            else { return false; }
        }
