#!/bin/bash


./head.cgi

disk_info() {
cat << EOF

   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Filesystem </a></th>
		<th><a class="text-info">Size </a></th>	
		<th><a class="text-info">Used </a></th>	
		<th><a class="text-info">Avail </a></th>	
		<th><a class="text-info">Use% </a></th>	
		<th><a class="text-info">Mounted on </a></th>			
      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`df -h | grep -v Filesystem \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $5 "<td bgcolor=white><font color=blue><h6><strong>" $6 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition"></div>
    <script type="text/javascript"><!--
        var pager = new Pager('results', 8); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF
echo "</section>"
}


mount_table() {
cat << EOF

   <table id="results2" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">Device</a></th>
		<th><a class="text-info">Mounted on</a></th>
		<th><a class="text-info">Type</a></th>
		<th><a class="text-info">Options</a></th>			
      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`mount \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $5 "<td bgcolor=white><font color=blue><h6><strong>" $6 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "</table>"

cat << EOF
<div id="pageNavPosition2"></div>
    <script type="text/javascript"><!--
        var pager2 = new Pager2('results2', 8); 
        pager2.init(); 
        pager2.showPageNav2('pager2', 'pageNavPosition2'); 
        pager2.showPage2(1);
    //--></script>
EOF
echo "</section>"
}


cat << EOF
                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- MEMORY RAM 
================================================== -->                      	

              <div class="panel panel-default">
                  <div class="panel-heading"><h4> Disk Usage </h4></div>
                  <div class="panel-body">

EOF

for i in `df -h | grep "^/" | awk '{print $6}'`; do

echo "<div><h5><p class="text-success"> `df -h $i  | awk '{print $1}' | awk 'NR==2'`<span class="text-info"> `df -h $i  | awk '{print $6}'`</p></h5>"
echo "<div class="progress">"

pr=`df -h $i  | awk '{print $5}' | awk 'NR==2'`

echo -e "<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow=" " aria-valuemin="0" aria-valuemax="100"  style="width:$pr">$pr</div> <p class="text-info" >  `df -h $i  | awk '{print $2}' | awk 'NR==2'` </p>"			
echo "</div>  </div> "         		
done	 

cat << EOF
           
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

</div><!--/row-->
<div>

                            <!-- SECOND RAW 
================================================== -->  
     
        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Disk Table</a>
             `disk_info` 
        </div>
      </div>

       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Mount - Table output</a>
            `mount_table`
        </div>
      </div>     
	  
      
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Disk Space - Raw output</a>
            <a  class="list-group-item"><pre><strong><font color=#2D3C86> `df -h` </font></strong></pre></a>
        </div>
      </div>

       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Disk - Raw output</a>
            <a  class="list-group-item"><pre><strong><font color=#2D3C86> `lsblk` </font></strong></pre></a>
        </div>
      </div>         

EOF
        
./footer.cgi
