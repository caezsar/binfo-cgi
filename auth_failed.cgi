#!/bin/bash



./head.cgi


invalid_auth() {
cat << EOF

   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">User</a></th>
		<th><a class="text-info">Date</a></th>
		<th><a class="text-info">Hour</a></th>
		<th><a class="text-info">From</a></th>		
		<th><a class="text-info">Protocol</a></th>		
      </tr>
    </thead>

EOF

echo "<tbody>"
echo "<tr bgcolor="#ffffff">"

item=`cat /var/log/auth.log | grep "Failed password" | grep invalid \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $11 "<td bgcolor=white><font color=blue><h6><strong>" $1 $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $13 "<td bgcolor=white><font color=blue><h6><strong>" $16 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition"></div>   
    <script type="text/javascript"><!--
        var pager = new Pager('results', 8); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF

echo "</section>"
}

invalid_auths() {
cat << EOF

<body onload="tableSearch.init();">
  
<input type="text" class="search1" size="auto" placeholder="Type value" id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />

   <table id="results1" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">User</a></th>
		<th><a class="text-info">Date</a></th>
		<th><a class="text-info">Hour</a></th>
		<th><a class="text-info">From</a></th>		
		<th><a class="text-info">Protocol</a></th>		
      </tr>
    </thead>

EOF

echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

item=`cat /var/log/auth.log | grep "Failed password"| grep -v invalid |grep -v message \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $9 "<td bgcolor=white><font color=blue><h6><strong>" $1 $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $11 "<td bgcolor=white><font color=blue><h6><strong>" $14 "</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

echo "  </table>"

cat << EOF
<div id="pageNavPosition1"></div>
    <script type="text/javascript"><!--
        var pager1 = new Pager1('results1', 8); 
        pager1.init(); 
        pager1.showPageNav1('pager1', 'pageNavPosition1'); 
        pager1.showPage1(1);
    //--></script>
EOF

echo "</section>"
}



cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- IP TABLES 
================================================== -->                      	

                  <div class="panel-body">
 
</div><!--/panel-body-->
</div><!--/panel-->                              
</div><!--/col-->

<div>

                            <!-- SECOND RAW 
================================================== -->  

      <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Failed SSH Attempts - System Users </a>
            `invalid_auths`
        </div>
      </div> 
	  
       <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Failed SSH Attempts - Invalid Users </a>
            `invalid_auth`
        </div>
      </div>  
	  	  
EOF
        
./footer.cgi
