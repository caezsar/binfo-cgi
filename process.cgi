#!/bin/bash


./head.cgi

### START TABLE FUNCTION ###
process() {
cat << EOF

<body onload="tableSearch.init();">
  
<input type="text"  class="search1" size="auto" placeholder="Type value" id="textBoxSearch" onkeyup="tableSearch.search(event);" />
<input type="button" class="search" value="Search" onclick="tableSearch.runSearch();" />
 
   <table id="results" class="table table-bordered">
    <thead>
      <tr bgcolor="#f4f4f4">
		<th><a class="text-info">User</a></th>
		<th><a class="text-info">Pid</a></th>
		<th><a class="text-info">CPU%</a></th>	
		<th><a class="text-info">MEM%</a></th>		
		<th><a class="text-info">RSS</a></th>	
		<th><a class="text-info">TTY</a></th>	
		<th><a class="text-info">Stat</a></th>	
		<th><a class="text-info">Command</a></th>	
      </tr>
    </thead>

EOF
##Start table with data##
echo "<tbody id="data">"
echo "<tr bgcolor="#ffffff">"

##Get rest of table from awk script###

item=`ps aux | grep -v USER| sed 's/<//g' \
| awk ' {}{line[j++] = "<td bgcolor=white><font color=blue><h6><strong>" $1 "<td bgcolor=white><font color=blue><h6><strong>" $2 "<td bgcolor=white><font color=blue><h6><strong>" $3 "<td bgcolor=white><font color=blue><h6><strong>" $4 "<td bgcolor=white><font color=blue><h6><strong>" $6 "<td bgcolor=white><font color=blue><h6><strong>" $7 "<td bgcolor=white><font color=blue><h6><strong>" $8 "<td bgcolor=white><font color=blue><h6><strong>" $11"</td></tr>"}END{ for(i=0;i<j;i++) print line[i]; }'`
echo $item

##Finish table###
echo "  </table>"

##Put pagination##
cat << EOF
<div id="pageNavPosition"></div>
    <script type="text/javascript"><!--
        var pager = new Pager('results', 10); 
        pager.init(); 
        pager.showPageNav('pager', 'pageNavPosition'); 
        pager.showPage(1);
    //--></script>
EOF

echo "</section>"
}
### END TABLES FUNCTION ###



cat << EOF

                            <!-- BEGIN CONTENT PAGE
================================================== -->     
              
                <div class="padding">
                    <div class="full">
                      
                        <!-- content -->                      
                      	<div class="row">
                      	
                      	
                            <!-- MEMORY RAM 
================================================== -->                      	
                      	
                
              <div class="panel panel-default">
                  <div class="panel-heading"><h4> Total Processes/User: `ps aux | wc -l` </h4></div>
                  <div class="panel-body">
     

EOF
     

users=`ps aux | grep -v USER | awk '{print $1}' | sort | uniq`
total_process=`ps aux | wc -l`

get_process() {
for i in $users; do
echo "<div align="right"><h6 id="progress-stacked"><p class="text-info"> `echo $i | awk '{print $1}'` </p></h6>"
echo "<div class="progress">"
user_process=`ps aux | grep $i | wc -l`


a=`echo $total_process`
b=`echo $user_process`

percent=`echo "$b*100/$a" | bc`

echo -e "<div class="progress-bar" style="width:"$percent%""><font color=#464646> $percent% </font></div><p class="text-danger"> $b </p>
 "		
echo "</div>  </div>"         		
done		 

}

## Activate Function
get_process


cat << EOF
           
</div><!--/panel-body-->
</div><!--/panel-->                                  
</div><!--/col-->
 </div><!--/row-->
 
                            <!-- SECOND RAW 
================================================== -->  

  <div class="row">
    
        <div class="panel-body">
          <div class="list-group">
            <a  class="list-group-item active">Processes Started: `ps aux | wc -l` - Table output</a>
           `process`
        </div>
      </div> 
 
        <div class="panel-body">
          <div class="list-group">

            <a  class="list-group-item active">Processes Started: pstree</a>
			<a  class="list-group-item">
		   <a  class="list-group-item"><pre><font color=#2D3C86 ><strong> `pstree -hcpnsu` </strong></font></pre></a>			

        </a></div>
      </div> 
	  
	  

EOF
        
./footer.cgi
